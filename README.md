# Contao FZ News list bundle
Format-Z Contao module for News list.

This module while not show the current news inside a news reader when showing similar news. There also is new fields :
* proposed by
* image for proposed by
* slogan

These three fields must be added on the news template to be used.

* proposed by => `$this->proposed_by`
* image proposed by => `{{image::<?= \StringUtil::binToUuid($this->proposed_by_img) ?>?width=60&height=60&class=img-circle}}`
* slogan => `$this->slogan`

# Installation
Edit the `composer.json` file and a new JSON block :
````json
"repositories": [
    {
        "type": "vcs",
        "url":  "https://gitlab.com/formatz/contao-fz-news-list-bundle"
    }
],
````

Then install the bundle with the command : `composer require formatz/contao-fz-news-list-bundle`
