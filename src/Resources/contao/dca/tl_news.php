<?php

// Modify the palette to add fields Proposed_by and Proposed_by_img
$GLOBALS['TL_DCA']['tl_news']['palettes']['default'] = str_replace
(
    'author',
    'author,proposed_by,proposed_by_img',
    $GLOBALS['TL_DCA']['tl_news']['palettes']['default']
);

// Add the field proposed by
$GLOBALS['TL_DCA']['tl_news']['fields']['proposed_by'] = array
(
    'label'     => &$GLOBALS['TL_LANG']['tl_news']['proposed_by'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'maxlength'=>155, 'tl_class'=>'w50 clr'),
    'sql'       => "varchar(155) NULL default ''"
);

// Add the file chooser for a proposed_by_img
$GLOBALS['TL_DCA']['tl_news']['fields']['proposed_by_img'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_news']['proposed_by_img'],
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'extensions'=>Config::get('validImageTypes'), 'fieldType'=>'radio', 'tl_class'=>'w50'),
    'sql'                     => "binary(16) NULL"
);

// Modify the palette to add fields slogan
$GLOBALS['TL_DCA']['tl_news']['palettes']['default'] = str_replace
(
    'subheadline',
    'subheadline,slogan',
    $GLOBALS['TL_DCA']['tl_news']['palettes']['default']
);

// Add the field proposed by
$GLOBALS['TL_DCA']['tl_news']['fields']['slogan'] = array
(
    'label'     => &$GLOBALS['TL_LANG']['tl_news']['slogan'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'maxlength'=>255),
    'sql'       => "varchar(255) NULL default ''"
);
