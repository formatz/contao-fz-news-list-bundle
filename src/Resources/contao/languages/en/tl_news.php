<?php

$GLOBALS['TL_LANG']['tl_news']['proposed_by'] = array
(
    'Propsed by',
    'Author of the news shown in the website',
);
$GLOBALS['TL_LANG']['tl_news']['proposed_by_img'] = array
(
    'Proposed by image',
    'Image of author of the news shown in the website (recommended size 60x60 px)',
);

$GLOBALS['TL_LANG']['tl_news']['slogan'] = array
(
    'Slogan',
    'Add a slogan for a news',
);
