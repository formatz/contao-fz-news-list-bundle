<?php

$GLOBALS['TL_LANG']['tl_news']['proposed_by'] = array
(
    'Proposé par',
    'Auteur de la news affiché sur le site',
);
$GLOBALS['TL_LANG']['tl_news']['proposed_by_img'] = array
(
    'Proposé par photo',
    'Photo de l\'auteur de la news affiché sur le site (taille conseillée 60x60 px)',
);

$GLOBALS['TL_LANG']['tl_news']['slogan'] = array
(
    'Slogan',
    'Ajoute un slogan pour une news',
);
