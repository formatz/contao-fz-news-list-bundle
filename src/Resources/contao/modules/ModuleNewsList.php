<?php
/**
 * Created by PhpStorm.
 * User: cedricbapst
 * Date: 29.05.19
 * Time: 10:20
 */

namespace ContaoFormatzNewsListBundle;


use Contao\Config;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\Environment;
use Contao\Input;
use Contao\Model\Collection;
use Contao\Pagination;

class ModuleNewsList extends \Contao\ModuleNewsList
{

    protected function compile()
    {
        // Get the news current item if the NewsList module is inside a NewsReader
        $objArticle = \NewsModel::findPublishedByParentAndIdOrAlias(\Input::get('items'), $this->news_archives);

        /*
         * If $objArticle is null = The module is not inside a NewsReader else we want to verify if the current item is
        inside the result of items list
        */
        if ($objArticle !== null) {
            // Get current item ID
            $articleId = $objArticle->id;

            $limit = null;
            $offset = (int) $this->skipFirst;

            // Maximum number of items
            if ($this->numberOfItems > 0)
            {
                $limit = $this->numberOfItems;
            }

            // Handle featured news
            if ($this->news_featured == 'featured')
            {
                $blnFeatured = true;
            }
            elseif ($this->news_featured == 'unfeatured')
            {
                $blnFeatured = false;
            }
            else
            {
                $blnFeatured = null;
            }

            $this->Template->articles = array();
            $this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];

            // Get the total number of items
            $intTotal = $this->countItems($this->news_archives, $blnFeatured);

            if ($intTotal < 1)
            {
                return;
            }

            $total = $intTotal - $offset;

            // Split the results
            if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
            {
                // Adjust the overall limit
                if (isset($limit))
                {
                    $total = min($limit, $total);
                }

                // Get the current page
                $id = 'page_n' . $this->id;
                $page = Input::get($id) ?? 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
                {
                    throw new PageNotFoundException('Page not found: ' . Environment::get('uri'));
                }

                // Set limit and offset
                $limit = $this->perPage;
                $offset += (max($page, 1) - 1) * $this->perPage;
                $skip = (int) $this->skipFirst;

                // Overall limit
                if ($offset + $limit > $total + $skip)
                {
                    $limit = $total + $skip - $offset;
                }

                // Add the pagination menu
                $objPagination = new Pagination($total, $this->perPage, Config::get('maxPaginationLinks'), $id);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }

            // Take one more than the limit
            $objArticles = $this->fetchItems($this->news_archives, $blnFeatured, ($limit + 1 ?: 0), $offset);

            if ($objArticles !== null) {
                $objArticlesNew = [];

                // Verify if the current article exist in the fetch result, if yes do not keep it
                $objArticleExist = false;
                foreach ($objArticles as $key => $article) {
                    if ($article->id === $articleId) {
                        $objArticleExist = true;
                    } else {
                        $objArticlesNew[] = $article;
                    }
                }

                // If current article did not exist in the list we need to remove one object
                if (!$objArticleExist) {
                    $objArticlesNew = [];
                    foreach ($objArticles as $key => $article) {
                        if ($key !== (int) $limit) {
                            $objArticlesNew[] = $article;
                        }
                    }
                }
                $this->Template->articles = $this->parseArticles(new Collection($objArticlesNew, 'tl_news'));
            }

            $this->Template->archives = $this->news_archives;

        } else {
            parent::compile();
        }
    }
}
